This note is based on the following [link](https://frappeframework.com/docs/user/en/installation). Some modifications have been made based on my experience.

This works for ubuntu linux on WSL2 - Windows 10.

Pls install WSL2 in Windows 10. And install Ubuntu LTS.  

Follow the instructions below to install erpnext.  

## 1. Install git, python, and redis

```
sudo apt install git python-dev redis-server
```
## 2. Install MariaDB
Please refer to this [link](https://downloads.mariadb.org/mariadb/repositories/#distro=Ubuntu&distro_release=focal--ubuntu_focal&mirror=piconets&version=10.5)


Here are the commands to run to install MariaDB 10.5 from the MariaDB repository on your Ubuntu system:

```
sudo apt-get install software-properties-common
sudo apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'
sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] https://mirrors.piconets.webwerks.in/mariadb-mirror/repo/10.5/ubuntu focal main'
```
Once the key is imported and the repository added you can install MariaDB 10.5 from the MariaDB repository with:

```
sudo apt update
sudo apt install mariadb-server
```
This installation does not allow setting password. Once installation is done follow these steps.

```
sudo service mariadb start
```

Run mysql secure installation. This will take you through steps to secure your installation. 
```
mysql_secure_installation
```
It is really important that you remember this password, since it'll be useful later on. 

Restart Maraidb service
```
sudo service mariadb restart
```

It is really important that you remember this password, since it'll be useful later on. 

```
apt-get install libmysqlclient-dev
```

Now, edit the MariaDB configuration file.

```
sudo nano /etc/mysql/my.cnf
```

And add this configuration.

```
[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci

[mysql]
default-character-set = utf8mb4
```

This my.cnf file also has includes. We need to make changes for the same. 

Open 50-server.cnf file in mariadb.conf.d folder.
```
sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf
```
Find line for **collation-server** and change it to ***utf8mb4_unicode_ci***.

Restart Maraidb service
```
sudo service mariadb restart
```

## 3. Install Node

First install [nvm](https://github.com/nvm-sh/nvm)

Run the following command

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
```

### Caution

*On Linux, after running the install script, if you get nvm: command not found or see no feedback from your terminal after you type command -v nvm, simply close your current terminal, open a new terminal, and try verifying again.*

Install node.

To download, compile, and install the latest release of node, do this:

```
nvm install node
```

Finally, install yarn using npm.

```
npm install -g yarn
```

Install wkhtmltopdf
```
apt-get install xvfb libfontconfig wkhtmltopdf
```

Install pip3 for python

```
sudo apt-get -y install python3-pip
```

## 4. Install Bench cli 

Run the following command.
```
pip3 install frappe-bench
```

Confirm the bench installation by checking version
```
bench --version

# output
5.2.1
```
Create your first bench folder.

```
cd ~
bench init frappe-bench
```

In case you want to install specific versions.
```
bench init frappe-bench --frappe-branch version-12
```

Install ERPnext
```
bench get-app erpnext https://github.com/frappe/erpnext
```

In case you want to install specific versions.
```
bench get-app erpnext --branch version-12 
```

Bench creates new user called frappe. In case you are running bench under a different user name you also have to change ownership for the files. You can run the following command.

```
cd frappe-bench
chown -R user:user *
```
In the above replace user with the name of the user running bench in both instances. Eg - *vijay:vijay* 

Create a new site

```
bench new-site erp.credence
```

Install erpnext 

```
bench --site erp.credence install-app erpnext
```

With the latest version of erpnext there are problems in building the files. Run bench build command. 

```
bench build
```

This may give error. Especially in not finding vuejs files. If you find this error cd to the folder frappe-bench/apps/erpnext and run this command.

```
npm install
```

After this run bench start. 

```
bench start
```

This should work. 



